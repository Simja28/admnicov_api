<?php
namespace Database\Seeders;
use Illuminate\Database\Seeder;
use App\Models\Roles;
use Uuid;
class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles= new Roles();
        $roles->uuid =  Uuid::generate()->string;
        $roles->rol='Administrador';
        

        $roles->save();

        $roles= new Roles();
        $roles->uuid= Uuid::generate()->string;
        $roles->rol='Jefe de Carrera';
        

        $roles->save();

        $roles= new Roles();
        $roles->uuid=Uuid::generate()->string;
        $roles->rol='Profesor';
        
        
        $roles->save();
    }
}
