<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assignments', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid');

            $table->integer('people_assigned_id');
           // $table->foreign('people_assigned_id')->references('id')->on('peoples');

            $table->integer('people_id')->unsigned();
            $table->foreign('people_id')->references('id')->on('people');

            $table->integer('grade_id')->unsigned();
            $table->foreign('grade_id')->references('id')->on('grades');

            $table->integer('group_id')->unsigned();
            $table->foreign('group_id')->references('id')->on('groups');

            $table->integer('carrer_id')->unsigned();
            $table->foreign('carrer_id')->references('id')->on('carrers');

            $table->integer('modality_id')->unsigned();
            $table->foreign('modality_id')->references('id')->on('modalities');

            $table->integer('campus_id')->unsigned();
            $table->foreign('campus_id')->references('id')->on('campus');

            $table->integer('period_id')->unsigned();
            $table->foreign('period_id')->references('id')->on('periods');

            $table->string('year');
            $table->boolean('status');
            $table->timestamp('created_at')->nullable(false)->useCurrent();
            $table->timestamp('updated_at')->nullable(true);
            $table->timestamp('deleted_at')->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assignments');
    }
}
