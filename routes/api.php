<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GroupController;
use App\Http\Controllers\CampusController;
use App\Http\Controllers\CarrersController;
use App\Http\Controllers\ModalityController;
use App\Http\Controllers\StudentsController;
use App\Http\Controllers\AssignmentsController;
use App\Http\Controllers\PeriodController;
use App\Http\Controllers\RolesController;

use App\Http\Controllers\UserController;

Route::post('add/user', [UserController::class, 'addUser']);
Route::post('login', [UserController::class, 'authenticate']);

Route::group(['middleware' => ['jwt.verify']], function() {

    Route::post('user',[UserController::class, 'getAuthenticatedUser']);

});

Route::post('/group', [GroupController::class, 'create']);
Route::get('/groups', [GroupController::class, 'listar']);
Route::put('/group/{uuid}', [GroupController::class, 'update']);
Route::delete('/group/{uuid}', [GroupController::class, 'delete']);

Route::post('/campus', [CampusController::class, 'create']);
Route::get('/campus', [CampusController::class, 'listar']);
Route::put('/campus/{uuid}', [CampusController::class, 'update']);
Route::delete('/campus/{uuid}', [CampusController::class, 'delete']);

Route::post('/carrers', [CarrersController::class, 'create']);
Route::get('/carrers', [CarrersController::class, 'listar']);
Route::put('/carrers/{uuid}', [CarrersController::class, 'update']);
Route::delete('/carrers/{uuid}', [CarrersController::class, 'delete']);

Route::post('/modalities', [ModalityController::class, 'create']);
Route::get('/modalities', [ModalityController::class, 'list']);
Route::put('/modalities/{uuid}', [ModalityController::class, 'update']);
Route::delete('/modalities/{uuid}', [ModalityController::class, 'delete']);
Route::post('/students', [CarrersController::class, 'create']);

Route::post('/students', [StudentsController::class, 'create']);
Route::get('/students', [StudentsController::class, 'listar']);
Route::put('/students/{uuid}', [StudentsController::class, 'update']);
Route::delete('/students/{uuid}', [StudentsController::class, 'delete']);

Route::post('/assignments', [AssignmentsController::class, 'create']);
Route::get('/assignments', [AssignmentsController::class, 'listar']);
Route::put('/assignments/{uuid}', [AssignmentsController::class, 'update']);
Route::delete('/assignments/{uuid}', [AssignmentsController::class, 'delete']);
Route::post('/periods', [PeriodController::class, 'create']);
Route::get('/periods', [PeriodController::class, 'listar']);
Route::put('/periods/{uuid}', [PeriodController::class, 'update']);
Route::delete('/periods/{uuid}', [PeriodController::class, 'delete']);

Route::post('/roles', [ RolesController::class, 'create']);
Route::get('/roles', [RolesController::class, 'listar']);
Route::put('/roles/{uuid}', [RolesController::class, 'update']);
Route::delete('/roles/{uuid}', [RolesController::class, 'delete']);

