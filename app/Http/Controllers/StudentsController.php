<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\StudentsRepository;
use Uuid;

class StudentsController extends Controller
{
    protected $student_repository;

    public function __construct(StudentsRepository $student)
    {
        $this->student_repository = $student;
    }

    public function create(Request $request)
    {
        $uuid = Uuid::generate()->string;
        $co_evaluations_id = $request->input('co_evaluations_id');
        $persons_id = $request->input('persons_id');
        $email = $request->input('email');
        $enrollment = $request->input('enrollment');
        $team_number = $request->input('team_number');
        return response()->json($this->student_repository->create($uuid,$co_evaluations_id,$persons_id,$enrollment,$team_number));
    }

    public function update(Request $request, $uuid)
    {
        $co_evaluations_id = $request->input('co_evaluations_id');
        $persons_id = $request->input('persons_id');
        $email = $request->input('email');
        $enrollment = $request->input('enrollment');
        $team_number = $request->input('team_number');
        return response()->json($this->student_repository->update($uuid,$co_evaluations_id,$persons_id,$enrollment,$team_number));
    }

    public function listar()
    {
        return response()->json($this->student_repository->list());
    }

    public function delete($uuid)
    {
        return response()->json($this->student_repository->delete($uuid));
    }

    public function search(Request $request)
    {
        $search = $request->input('search');
        return response()->json($this->student_repository->search($search));
    }

    public function find($uuid)
    {
        return response()->json($this->student_repository->find($uuid));
    }

}
