<?php

namespace App\Http\Controllers;

use App\Repositories\ModalityRepository;
use Illuminate\Http\Request;
use Uuid;

class ModalityController extends Controller
{
    protected $modality_repository;

    public function __construct(ModalityRepository $modality)
    {
        $this->modality_repository = $modality;
    }

    public function create(Request $request)
    {
        $uuid = Uuid::generate()->string;
        $name = $request->input('name');
        $alias = $request->input('alias');
        return response()->json($this->modality_repository->create($uuid,$name,$alias));
    }

    public function update(Request $request, $uuid)
    {
        $name = $request->input('name');
        $alias = $request->input('alias');
        return response()->json($this->modality_repository->update($uuid, $name,$alias));
    }

    public function list()
    {
        return response()->json($this->modality_repository->list());
    }

    public function delete($uuid)
    {
        return response()->json($this->modality_repository->delete($uuid));
    }

    public function search(Request $request)
    {
        $search = $request->input('search');
        return response()->json($this->modality_repository->search($search));
    }

    public function find($uuid)
    {
        return response()->json($this->modality_repository->find($uuid));
    }
}
