<?php

namespace App\Http\Controllers;

use App\Repositories\PeriodRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Uuid;

class PeriodController extends Controller
{
    protected $period_repository;

    public function __construct(PeriodRepository $period)
    {
        $this->period_repository = $period;
    }

    public function create(Request $request)
    {
		$validator = Validator::make($request->all(), [
        'name' => 'required',
        'alias' => 'required|integer'
		]);

		if ($validator->fails()) {
			return response()->json(['error'=>$validator->errors()], 401);
		}
		
        $uuid = Uuid::generate()->string;
        $name = $request->input('name');
        $alias = $request->input('alias');
        return response()->json($this->period_repository->create($uuid,$name,$alias));
    }

    public function update(Request $request, $uuid)
    {
		$validator = Validator::make($request->all(), [
        'name' => 'required',
        'alias' => 'required|integer'
		]);

		if ($validator->fails()) {
			return response()->json(['error'=>$validator->errors()], 401);
		}
		
        $name = $request->input('name');
        $alias = $request->input('alias');
        return response()->json($this->period_repository->update($uuid, $name,$alias));
    }

    public function listar()
    {
        return response()->json($this->period_repository->list());
    }

    public function delete($uuid)
    {
        return response()->json($this->period_repository->delete($uuid));
    }

    public function search(Request $request)
    {
        $search = $request->input('search');
        return response()->json($this->period_repository->search($search));
    }

    public function find($uuid)
    {
        return response()->json($this->period_repository->find($uuid));
    }



}
