<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\CampusRepository;
<<<<<<< HEAD
use Uuid; //Llamar la librería del controlador
=======
use Uuid;
>>>>>>> a4e844ffac9f101253a81d58f2d01ff4ba1b7940

class CampusController extends Controller
{
    protected $campus_repository;

    public function __construct(CampusRepository $campus)
    {
        $this->campus_repository = $campus;
    }

    public function create(Request $request)
    {
<<<<<<< HEAD
        $uuid = Uuid::generate()->string; //Genera una credencial unica 
=======
        $uuid = Uuid::generate()->string;
>>>>>>> a4e844ffac9f101253a81d58f2d01ff4ba1b7940
        $name = $request->input('name');
        $alias = $request->input('alias');
        return response()->json($this->campus_repository->create($uuid,$name,$alias));
    }

    public function update(Request $request, $uuid)
    {
        $name = $request->input('name');
        $alias = $request->input('alias');
        return response()->json($this->campus_repository->update($uuid, $name,$alias));
    }

    public function listar()
    {
        return response()->json($this->campus_repository->list());
    }

    public function delete($uuid)
    {
        return response()->json($this->campus_repository->delete($uuid));
    }

    public function search(Request $request)
    {
        $search = $request->input('search');
        return response()->json($this->campus_repository->search($search));
    }

    public function find($uuid)
    {
        return response()->json($this->campus_repository->find($uuid));
    }
<<<<<<< HEAD
=======

>>>>>>> a4e844ffac9f101253a81d58f2d01ff4ba1b7940
}
