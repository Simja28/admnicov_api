<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\CarrersRepository;
use Uuid;

class CarrersController extends Controller
{
    protected $carrers_repository;

    public function __construct(CarrersRepository $carrers)
    {
        $this->carrers_repository = $carrers;
    }

    public function create(Request $request)
    {
        $uuid=Uuid::generate()->string;
        $name=$request->input('name');
        $alias=$request->input('alias');
        return response()->json($this->carrers_repository->create($uuid,$name,$alias));
    }

    public function update(Request $request, $uuid)
    {
        $name = $request->input('name');
        $alias = $request->input('alias');
        return response()->json($this->carrers_repository->update($uuid, $name,$alias));
    }

    public function listar()
    {
        return response()->json($this->carrers_repository->list());
    }

    public function delete($uuid)
    {
        return response()->json($this->carrers_repository->delete($uuid));
    }

    public function search(Request $request)
    {
        $search = $request->input('search');
        return response()->json($this->carrers_repository->search($search));
    }

    public function find($uuid)
    {
        return response()->json($this->carrers_repository->find($uuid));
    }

}
