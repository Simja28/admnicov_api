<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\GroupRepository;
use Uuid;

class GroupController extends Controller
{
    protected $group_repository;

    public function __construct(GroupRepository $group)
    {
        $this->group_repository = $group;
    }

    public function create(Request $request)
    {
        $uuid = Uuid::generate()->string;
        $name = $request->input('name');
        return response()->json($this->group_repository->create($uuid,$name));
    }

    public function update(Request $request, $uuid)
    {
        $name = $request->input('name');
        return response()->json($this->group_repository->update($uuid, $name));
    }

    public function listar()
    {
        return response()->json($this->group_repository->list());
    }

    public function delete($uuid)
    {
        return response()->json($this->group_repository->delete($uuid));
    }

    public function search(Request $request)
    {
        $search = $request->input('search');
        return response()->json($this->group_repository->search($search));
    }

    public function find($uuid)
    {
        return response()->json($this->group_repository->find($uuid));
    }

}
