<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\AssignmentsRepository;
use Uuid;

class AssignmentsController extends Controller
{
    protected $assignments_repository;

    public function __construct(AssignmentsRepository $assignments)
    {
        $this->assignments_repository = $assignments;
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'people_assigned_id' => 'required',
            'people_id' => 'required',
            'grade_id' => 'required',
            'group_id' => 'required',
            'carrer_id' => 'required',
            'modality_id' => 'required',
            'campus_id' => 'required',
            'period_id' => 'required',
            'year' => 'required',
            'status' => 'required'
            ]);
            if ($validator->fails()) {
                return response()->json(['error'=>$validator->errors()], 401);
            }
        
        $uuid = Uuid::generate()->string;
        $people_assigned_id = $request->input('people_assigned_id');
        $people_id = $request->input('people_id');
        $grade_id = $request->input('grade_id');
        $group_id = $request->input('group_id');
        $carrer_id = $request->input('carrer_id');
        $modality_id = $request->input('modality_id');
        $campus_id = $request->input('campus_id');
        $period_id = $request->input('period_id');
        $year = $request->input('year');
        $status = $request->input('status');
        return response()->json($this->assignment_repository->create($uuid, $people_assigned_id, $people_id, $grade_id, $group_id, $carrer_id, $modality_id, $campus_id, $period_id, $year, $status));
    }
    
    public function update(Request $request, $uuid)
    {
        $people_assigned_id = $request->input('people_assigned_id');
        $people_id = $request->input('people_id');
        $grade_id = $request->input('grade_id');
        $group_id = $request->input('group_id');
        $carrer_id = $request->input('carrer_id');
        $modality_id = $request->input('modality_id');
        $campus_id = $request->input('campus_id');
        $period_id = $request->input('period_id');
        $year = $request->input('year');
        $status = $request->input('status');
        return response()->json($this->assignment_repository->update($uuid, $people_assigned_id, $people_id, $grade_id, $group_id, $carrer_id, $modality_id, $campus_id, $period_id, $year, $status));

    }

    public function listar()
    {
        return response()->json($this->assignment_repository->list());
    }

    public function delete($uuid)
    {
        return response()->json($this->assignment_repository->delete($uuid));
    }

    public function search(Request $request)
    {
        $search = $request->input('search');
        return response()->json($this->assignment_repository->search($search));
    }

    public function find($uuid)
    {
        return response()->json($this->assignment_repository->find($uuid));
    }



}
