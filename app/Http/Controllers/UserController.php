<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $user;

    public function __construct(UserRepository $user)
    {
        $this->user = $user;
    }

    public function authenticate(Request $request)
    {
        return $this->user->authenticate($request);
    }

    public function getAuthenticatedUser()
    {
        return $this->user->getAuthenticatedUser();
    }

    public function addUser(Request $request)
    {
        return $this->user->addUser($request);
    }
}
