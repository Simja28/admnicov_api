<?php
namespace App\Repositories;
use App\Models\Group;

class GroupRepository
{
    public function create($uuid,$name)
    {
        $nuevo_grupo['uuid']=$uuid;
        $nuevo_grupo['name']=$name;
        return Group::create($nuevo_grupo);
    }

    public function update($uuid,$name)
    {
        $grupo = $this->find($uuid);
        $grupo->name = $name;
        return $grupo->save();
    }

    public function delete($uuid)
    {
        $grupo = $this->find($uuid);
        return $grupo->delete();
    }

    public function find($uuid)
    {
        return Group::where('uuid', '=', $uuid)->first();
    }

    public function search($busqueda)
    {
        return Group::where('name', 'like', '%'.$busqueda.'%')->get();
    }

    public function list()
    {
        return Group::all();
    }


}
?>
