<?php
namespace App\Repositories;
use App\Models\Carrer;

class CarrersRepository
{
    public function create($uuid,$name,$alias)
    {
        $Nuevo_carrers['uuid']=$uuid;
        $Nuevo_carrers['name']=$name;
        $Nuevo_carrers['alias']=$alias;
        return Carrer::create($Nuevo_carrers);
    }

    public function update($uuid,$name,$alias)
    {
        $carrers = $this->find($uuid);
        $carrers->name = $name;
        $carrers->alias= $alias;
        $carrers->save();
        return $carrers;
    }

    public function delete($uuid)
    {
        $carrers = $this->find($uuid);
        return $carrers->delete();
    }

    public function find($uuid)
    {
        return Carrer::where('uuid', '=', $uuid)->first();
    }

    public function search($busqueda)
    {
        return Carrer::where('name', 'like', '%'.$busqueda.'%')->get();
    }

    public function list()
    {
        return Carrer::all();
    }


}
