<?php
namespace App\Repositories;
use App\Models\Students;

class StudentsRepository
{
    public function create($uuid,$co_evaluations_id,$persons_id,$email,$enrollment,$team_number)
    {
        $Nuevo_student['uuid']=$uuid;
        $Nuevo_student['co_evaluations_id']=$co_evaluations_id;
        $Nuevo_student['persons_id']=$persons_id;
        $Nuevo_student['email']=$email;
        $Nuevo_student['enrollment']=$enrollment;
        $Nuevo_student['team_number']=$team_number;

        return Students::create($Nuevo_student);
    }

    public function update($uuid,$co_evaluations_id,$persons_id,$email,$enrollment,$team_number)
    {
        $student = $this->find($uuid);
        $student->co_evaluations_id = $co_evaluations_id;
        $student->persons_id= $persons_id;
        $student->email= $email;
        $student->enrollment= $enrollment;
        $student->team_number= $team_number;
        $student->save();
        return $student;
    }

    public function delete($uuid)
    {
        $student = $this->find($uuid);
        return $student->delete();
    }

    public function find($uuid)
    {
        return Students::where('uuid', '=', $uuid)->first();
    }

    public function search($busqueda)
    {
        return Students::where('email', 'like', '%'.$busqueda.'%')->get();
    }

    public function list()
    {
        return Students::all();
    }


}
