<?php
namespace App\Repositories;
use App\Models\Assignment;

class AssignmentsRepository
{
    public function create($uuid, $people_assigned_id, $people_id, $grade_id, $group_id, $carrer_id, $modality_id, $campus_id, $period_id, $year, $status)
    {
        $Nuevo_assignments['uuid']=$uuid;
        $Nuevo_assignments['people_assigned_id']=$people_assigned_id;
        $Nuevo_assignments['people_id']=$people_id;
		$Nuevo_assignments['grade_id']=$grade_id;
        $Nuevo_assignments['group_id']=$group_id;
		$Nuevo_assignments['carrer_id']=$carrer_id;
        $Nuevo_assignments['modality_id']=$modality_id;
		$Nuevo_assignments['campus_id']=$campus_id;
        $Nuevo_assignments['period_id']=$period_id;
        $Nuevo_assignments['year']=$year;
		$Nuevo_assignments['status']=$status;

        return Assignment::create($Nuevo_assignments);
    }

    public function update($uuid, $people_assigned_id, $people_id, $grade_id, $group_id, $carrer_id, $modality_id, $campus_id, $period_id, $year, $status)
    {
        $assignments = $this->find($uuid);
        $assignments->people_assigned_id = $people_assigned_id;
        $assignments->people_id= $people_id;
		$assignments->grade_id = $grade_id;
        $assignments->group_id= $group_id;
		$assignments->carrer_id = $carrer_id;
        $assignments->modality_id= $modality_id;
		$assignments->campus_id = $campus_id;
        $assignments->period_id= $period_id;
		$assignments->year = $year;
        $assignments->status= $status;
        $assignments->save();
        return $assignments;
    }

    public function delete($uuid)
    {
        $assignments = $this->find($uuid);
        return $assignments->delete();
    }

    public function find($uuid)
    {
        return Assignment::where('uuid', '=', $uuid)->first();
    }

    public function search($busqueda)
    {
        return Assignment::where('year', 'like', '%'.$busqueda.'%')->get();
    }

    public function list()
    {
		$assignment = Assignment::with('people', 'grades', 'group', 'carrer', 'modalities', 'campus', 'periods')->get();
        return $assignment->toArray();
    }


}
