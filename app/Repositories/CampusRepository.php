<?php
namespace App\Repositories;
use App\Models\Campus;

class CampusRepository
{
    public function create($uuid,$name,$alias)
    {
        $Nuevo_campus['uuid']=$uuid;
        $Nuevo_campus['name']=$name;
        $Nuevo_campus['alias']=$alias;
        return Campus::create($Nuevo_campus);
    }

    public function update($uuid,$name,$alias)
    {
        $campus = $this->find($uuid);
        $campus->name = $name;
        $campus->alias= $alias;
        $campus->save();
        return $campus;
    }

    public function delete($uuid)
    {
        $campus = $this->find($uuid);
        return $campus->delete();
    }

    public function find($uuid)
    {
        return Campus::where('uuid', '=', $uuid)->first();
    }

    public function search($busqueda)
    {
        return Campus::where('name', 'like', '%'.$busqueda.'%')->get();
    }

    public function list()
    {
        return Campus::all();
    }


}
