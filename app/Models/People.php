<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class People extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'first_name',
        'second_name',
    ];

    public function user()
    {
        return $this->hasOne('App\Models\User');
    }
}
