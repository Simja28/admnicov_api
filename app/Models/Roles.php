<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Uuid;

class Roles extends Model
{
    use softDeletes;
    protected $table = 'roles';
    protected $filleable = [
'uuid','rol'
   ];
   protected $hidden = [
    'created_at','updated_at','deleted_at'
   ];

   public function user(){
       return $this->belongsToMany(User::class);
   }
}

