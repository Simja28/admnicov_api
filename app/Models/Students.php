<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Students extends Model
{
    use softDeletes;
    protected $table = 'Students';
    protected $fillable = [
        'uuid','co_evaluations_id', 'persons_id','email','enrollment','team_number'
    ];
    protected $hidden = [
        'created_at','updated_at','deleted_at'
    ];
    public function persons(){
        return $this->belongsTo(Persons::class, 'id', 'persons_id');

    }
    public function coevaluations(){
        return $this->belongsTo(Co_evaluations_id::class, 'id', 'co_evaluations_id');

    }
}
