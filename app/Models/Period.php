<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Period extends Model
{
    use softDeletes;
    protected $table = 'periods';
    protected $fillable = [
        'uuid','name','alias'
    ];
    protected $hidden = [
        'created_at','updated_at','deleted_at'
    ];
}
