<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Assignment extends Model
{
    use softDeletes;
    protected $table = 'assignments';
    protected $fillable = [
        'uuid',
        'people_assigned_id',
        'people_id', 
        'grade_id',
        'group_id',
        'carrer_id', 
        'modality_id', 
        'campus_id', 
        'period_id',
        'year',
        'status'
    ];
    protected $hidden = [
        'created_at','updated_at','deleted_at'
    ];
    
    public function people(){
        return $this->belongsTo(People::class, 'people_id', 'id' );
    }
    public function grades(){
        return $this->belongsTo(Grade::class, 'grade_id', 'id' );
    }
    public function group(){
        return $this->belongsTo(Group::class, 'group_id', 'id' );
    }
    public function carrer(){
        return $this->belongsTo(Carrer::class, 'carrer_id', 'id' );
    }
    public function modalities(){
        return $this->belongsTo(Modality::class, 'modality_id', 'id' );
    }
    public function campus(){
        return $this->belongsTo(Campus::class, 'campus_id', 'id' );
    }
    public function periods(){
        return $this->belongsTo(Period::class, 'period_id', 'id' );
    }
}
